package weeded

import (
	"bytes"
)

func Abs(v int) int {
	if v >= 0 {
		return v
	}
	return -v
}

func IndexAll(b []byte, sep []byte) []int {
	var offset int
	var indices []int
	for ix := bytes.Index(b, sep); ix >= 0; ix = bytes.Index(b, sep) {
		indices = append(indices, ix+offset)
		b = b[ix+len(sep):]
		offset += ix + len(sep)
	}
	return indices
}
