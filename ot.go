package weeded

import (
	"bytes"
	"errors"
)

type SubOp struct {
	N int
	S []byte
}

type Op []SubOp

func (sop SubOp) IsInsert() bool {
	return sop.N > 0 && len(sop.S) > 0
}

func (sop SubOp) IsDelete() bool {
	return sop.N < 0
}

func (sop SubOp) IsRetain() bool {
	return sop.N > 0 && len(sop.S) == 0
}

func (sop SubOp) IsNoop() bool {
	return sop.N == 0
}

func (op Op) Insert(s []byte) Op {
	return append(op, SubOp{N: len(s), S: s})
}

func (op Op) Delete(s []byte) Op {
	return append(op, SubOp{N: -len(s), S: s})
}

func (op Op) Retain(n int) Op {
	return append(op, SubOp{N: n})
}

func (op Op) Count() (ret, del, ins int) {
	for _, sop := range op {
		switch {
		case sop.IsRetain():
			ret += sop.N
		case sop.IsDelete():
			del += -sop.N
		case sop.IsInsert():
			ins += sop.N
		}
	}
	return
}

func (op Op) Equals(other Op) bool {
	if len(op) != len(other) {
		return false
	}
	for i, o := range other {
		if op[i].N != o.N || !bytes.Equal(op[i].S, o.S) {
			return false
		}
	}
	return true
}

func (op Op) Squeeze() Op {
	var ret Op

	for _, sop := range op {
		if sop.IsNoop() {
			continue
		}
		i := len(ret) - 1
		var lastOp SubOp
		if i > -1 {
			lastOp = ret[i]
		}

		switch {
		case lastOp.IsRetain() && sop.IsRetain():
			ret[i].N += sop.N
		case lastOp.IsDelete() && sop.IsDelete():
			ret[i].N += sop.N
			ret[i].S = append(ret[i].S, sop.S...)
		case lastOp.IsInsert() && sop.IsInsert():
			ret[i].N += sop.N
			ret[i].S = append(ret[i].S, sop.S...)
		case lastOp.IsDelete() && sop.IsInsert():
			//insert always before delete
			ret[i] = sop
			ret = append(ret, lastOp)
		default:
			ret = append(ret, sop)
		}
	}
	return ret
}

func (op Op) Inverse() Op {
	inv := make(Op, len(op))
	copy(inv, op)
	for i, sop := range inv {
		if sop.IsInsert() || sop.IsDelete() {
			inv[i].N = -sop.N
		}
	}
	return inv
}

func (op Op) UpdateIx(ix int) int {
	docIx := 0
	for _, sop := range op {
		if docIx > ix {
			return ix
		}
		if sop.IsInsert() {
			docIx += sop.N
			ix += sop.N
		} else {
			docIx += Abs(sop.N)
		}
	}
	return ix
}

func subop(op Op, i int) (SubOp, int) {
	if i >= 0 && i < len(op) {
		return op[i], i + 1
	}
	return SubOp{}, i
}

func Transform(a, b Op) (at Op, bt Op, err error) {
	if len(a) == 0 || len(b) == 0 {
		return a, b, nil
	}
	reta, dela, _ := a.Count()
	retb, delb, _ := b.Count()

	if reta+dela != retb+delb {
		return nil, nil, errors.New("base lengths have to match")
	}

	ia, ib := 0, 0
	a = a.Squeeze()
	b = b.Squeeze()

	opa, ia := subop(a, ia)
	opb, ib := subop(b, ib)

	for {
		if opa.IsNoop() && opb.IsNoop() {
			return
		}

		if opa.IsInsert() {
			at = append(at, opa)
			bt = bt.Retain(opa.N)

			opa, ia = subop(a, ia)
			continue
		}
		if opb.IsInsert() {
			at = at.Retain(opb.N)
			bt = append(bt, opb)

			opb, ib = subop(b, ib)
			continue
		}

		switch {
		case opa.IsRetain() && opb.IsRetain():
			minl := 0
			switch {
			case opa.N > opb.N:
				minl = opb.N
				opa.N -= opb.N

				opb, ib = subop(b, ib)
			case opa.N == opb.N:
				minl = opb.N

				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case opa.N < opb.N:
				minl = opa.N
				opb.N -= opa.N

				opa, ia = subop(a, ia)
			}
			at = at.Retain(minl)
			bt = bt.Retain(minl)
		case opa.IsDelete() && opb.IsDelete():
			switch {
			case opa.N < opb.N:
				opa.N -= opb.N
				opa.S = opa.S[-opb.N:]

				opb, ib = subop(b, ib)
			case opa.N == opb.N:
				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case opa.N > opb.N:
				opb.N -= opa.N
				opb.S = opb.S[-opa.N:]

				opa, ia = subop(a, ia)
			}
		case opa.IsDelete() && opb.IsRetain():
			switch {
			case -opa.N > opb.N:
				at = at.Delete(opa.S[:opb.N])
				bt = append(bt, opb)

				opa.N += opb.N
				opa.S = opa.S[opb.N:]
				opb, ib = subop(b, ib)
			case -opa.N == opb.N:
				at = append(at, opa)
				bt = append(bt, opb)

				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case -opa.N < opb.N:
				at = append(at, opa)
				bt = bt.Retain(-opa.N)

				opb.N += opa.N
				opa, ia = subop(a, ia)
			}
		case opa.IsRetain() && opb.IsDelete():
			switch {
			case opa.N < -opb.N:
				at = append(at, opa)
				bt = bt.Delete(opb.S[:opa.N])

				opb.N += opa.N
				opb.S = opb.S[opa.N:]
				opa, ia = subop(a, ia)
			case opa.N == -opb.N:
				at = append(at, opa)
				bt = append(bt, opb)

				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case opa.N > -opb.N:
				at = at.Retain(-opb.N)
				bt = append(bt, opb)

				opa.N += opb.N
				opb, ib = subop(b, ib)
			}
		default:
			panic("unreachable")
		}
	}
}

/*
//working in priciple, but needs a way to insert tombstones as subop
func Compose(a, b Op) (Op, error) {

	if len(a) == 0 {
		return b, nil
	}
	if len(b) == 0 {
		return a, nil
	}

	var ab Op

	a = a.Squeeze()
	b = b.Squeeze()

	reta, dela, ins := a.Count()
	retb, delb, _ := b.Count()

	if reta+dela+ins != retb+delb {
		return ab, errors.New("Compose requires consecutive ops")
	}

	ia, ib := 0, 0

	opa, ia := subop(a, ia)
	opb, ib := subop(b, ib)

	for {
		if opa.IsNoop() && opb.IsNoop() {
			return ab, nil
		}

		if opb.IsInsert() {
			ab = append(ab, opb)

			opb, ib = subop(b, ib)
			continue
		}

		switch {
		case opa.IsRetain() && opb.IsRetain():
			switch {
			case opa.N > opb.N:
				ab = append(ab, opb)

				opa.N -= opb.N
				opb, ib = subop(b, ib)
			case opa.N == opb.N:
				ab = append(ab, opb)

				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case opa.N < opb.N:
				ab = append(ab, opa)

				opb.N -= opa.N
				opa, ia = subop(a, ia)
			}
		case opa.IsInsert() && opb.IsDelete():
			// need to insert tombstones to match up with later
			// operations ....
			switch {
			case opa.N > -opb.N:
				opa.N += opb.N
				opa.S = opa.S[-opb.N:]

				opb, ib = subop(b, ib)
			case opa.N == -opb.N:
				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case opa.N < -opb.N:
				opb.N += opa.N
				opb.S = opb.S[opa.N:]

				opa, ia = subop(a, ia)
			}
		case opa.IsInsert() && opb.IsRetain():
			switch {
			case opa.N > opb.N:
				ab = ab.Insert(opa.S[:opb.N])
				opa.N -= opb.N
				opa.S = opa.S[opb.N:]

				opb, ib = subop(b, ib)
			case opa.N == opb.N:
				ab = append(ab, opa)

				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case opa.N < opb.N:
				ab = append(ab, opa)
				opb.N -= opa.N

				opa, ia = subop(a, ia)
			}
		case opa.IsRetain() && opb.IsDelete():
			switch {
			case opa.N > -opb.N:
				ab = append(ab, opb)
				opa.N += opb.N

				opb, ib = subop(b, ib)
			case opa.N == -opb.N:
				ab = append(ab, opb)

				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case opa.N < -opb.N:
				ab = ab.Delete(opb.S[:opa.N])
				opb.N += opa.N
				opb.S = opb.S[opa.N:]

				opa, ia = subop(a, ia)
			}
		case opa.IsDelete() && opb.IsDelete():
			ab = append(ab, opa)
			opa, ia = subop(a, ia)
		case opa.IsDelete() && opb.IsRetain():
			switch {
			case -opa.N > opb.N:
				ab.Delete(opa.S[:opb.N])

				opa.S = opa.S[opb.N:]
				opa.N += opb.N
				opb, ib = subop(b, ib)
			case -opa.N == opb.N:
				ab = append(ab, opa)

				opa, ia = subop(a, ia)
				opb, ib = subop(b, ib)
			case -opa.N < opb.N:
				ab = append(ab, opa)

				opb.N += opa.N
				opa, ia = subop(a, ia)
			}
		default:
			panic("unreachable")
		}

	}
	ab = ab.Squeeze()
	return ab, nil
}
*/
