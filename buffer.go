package weeded

import (
	"os"

	mmap "github.com/edsrzf/mmap-go"
)

type OpMsg struct {
	ID uint32
	Ix int64
	Op Op
}

type PieceChainView struct {
	pc *PieceChain
	ix int64
	b  *Buffer
}

type Update struct {
	pv PieceChainView
	op Op
}

type Buffer struct {
	filename string
	pc       *PieceChain

	ops     chan OpMsg
	views   []chan Update
	connect chan chan Update

	data []byte

	nextID uint32
}

func NewBuffer(filename string) (*Buffer, error) {
	buf := &Buffer{
		filename: filename,
		ops:      make(chan OpMsg),
		connect:  make(chan chan Update),
	}
	if filename != "" {
		f, err := os.OpenFile(filename, os.O_RDONLY, 0)
		if err != nil {
			return nil, err
		}
		buf.data, err = mmap.Map(f, 0, mmap.RDONLY)
		if err != nil {
			return nil, err
		}
		buf.pc = NewPieceChain(buf.data)
	} else {
		buf.pc = NewPieceChain(nil)
	}

	go buf.run()

	return buf, nil
}

func (b *Buffer) run() {
	nextIx := int64(0)
	var hist []Op
	for {
		select {
		case opMsg := <-b.ops:
			op := opMsg.Op

			var err error
			for i := opMsg.Ix; i < nextIx; i++ {
				op, _, err = Transform(op, hist[i])
				if err != nil {
					panic(err)
				}
			}
			err = b.pc.Apply(op)
			if err != nil {
				panic(err)
			}
			hist = append(hist, op)
			for _, c := range b.views {
				c <- Update{
					pv: PieceChainView{b: b, ix: nextIx, pc: b.pc.View(0, b.pc.Length())},
					op: op,
				}
			}

		case c := <-b.connect:
			b.views = append(b.views, c)
			c <- Update{pv: PieceChainView{
				b:  b,
				pc: b.pc.View(0, b.pc.Length()),
				ix: nextIx,
			}}
		}
	}
}

func (b *Buffer) Apply(id uint32, ix int64, op Op) {
	b.ops <- OpMsg{ID: id, Ix: ix, Op: op}
}

func (pv PieceChainView) Apply(id uint32, op Op) {
	pv.b.Apply(id, pv.ix, op)
}

func (pv PieceChainView) NewReader() *PieceChainReader {
	return pv.pc.NewReader()
}

func (pv PieceChainView) Length() int {
	return pv.pc.Length()
}
