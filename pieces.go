package weeded

import (
	"bytes"
	"errors"
	"fmt"
	"sort"
)

type piece struct {
	Ix         int
	LineNumber int

	LineBreaks []int

	Dead bool
	Data []byte

	next *piece
	prev *piece
}

type PieceChain struct {
	head *piece
	tail *piece

	gap      int
	gapLines int
}

func NewPieceChain(initialData []byte) *PieceChain {
	head := &piece{Ix: 0}
	tail := &piece{Ix: 0}
	head.next = tail
	tail.prev = head

	if len(initialData) > 0 {
		tail.insertBefore(initialData, false)
	}

	return &PieceChain{head: head, tail: tail}
}

func (pc PieceChain) Printall() {
	p := pc.head
	for p != nil {
		fmt.Println(p)
		p = p.next
	}
}

func (pc PieceChain) View(from int, to int) *PieceChain {
	if from > to {
		panic("from should be smaller than to")
	}
	if from < 0 || to > pc.Length() {
		panic("index out of range")
	}
	p := pc.head

	for p.Ix+len(p.Data) <= from {
		p = p.next
	}
	head := &piece{Ix: from}
	tail := &piece{Ix: from}
	head.next = tail
	tail.prev = head

	relIx := from - p.Ix
	split := sort.SearchInts(p.LineBreaks, relIx)
	head.LineNumber = p.LineNumber + split
	tail.LineNumber = head.LineNumber

	tail.insertBefore(p.Data[from-p.Ix:], p.Dead)
	for p.Ix+len(p.Data) < to {
		p = p.next
		tail.insertBefore(p.Data, p.Dead)
	}
	tail.prev.Data = tail.prev.Data[:to-tail.prev.Ix]
	tail.Ix = to

	return &PieceChain{
		head:     head,
		tail:     tail,
		gap:      pc.Length() - to,
		gapLines: pc.NumLines() - tail.LineNumber,
	}
}

func (pc PieceChain) Length() int {
	return pc.tail.Ix + pc.gap
}

func (pc PieceChain) NumLines() int {
	return pc.tail.LineNumber + pc.gapLines
}

func (pc *PieceChain) Apply(op Op) error {

	p := pc.head

	docIx := 0
	for _, sop := range op {
		switch {
		case sop.IsRetain():
			docIx += sop.N
		case sop.IsInsert():
			if docIx < p.Ix {
				p.next.updateIndices(len(sop.S), bytes.Count(sop.S, []byte("\n")))
			} else {
				p = p.split(docIx)
				p.insertBefore(sop.S, false)
				p = p.prev
				if !p.prev.Dead && p.prev.Data != nil && len(p.prev.Data)+len(p.Data) < 2024 {
					p = p.mergeLeft()
				}
			}
			docIx += sop.N
		case sop.IsDelete():
			if docIx-sop.N <= p.Ix {
				docIx += sop.N
				continue
			}
			if docIx < p.Ix {
				diff := p.Ix - docIx
				docIx = p.Ix
				sop.S = sop.S[diff:]
				sop.N += diff
			}
			p = p.split(docIx)
			delTo := docIx - sop.N
			if delTo > pc.tail.Ix {
				delTo = pc.tail.Ix
			}
			docIx = delTo
			right := p.split(delTo)
			for p != right {
				if !bytes.Equal(p.Data, sop.S[:len(p.Data)]) {
					return errors.New("The string which should be deleted does not match the document.")
				}
				sop.S = sop.S[len(p.Data):]
				p.kill()
				if p.prev.Dead {
					p = p.mergeLeft()
				}
				p = p.next
			}
			if right.Dead {
				p = right.mergeLeft()
			}
		}
		if docIx > pc.Length() {
			break
		}
	}
	return nil
}

func (p *piece) split(ix int) *piece {
	if ix < p.Ix {
		panic("index too small")
	}
	for ix >= p.Ix+len(p.Data) && p.next != nil {
		p = p.next
	}
	if ix == p.Ix {
		return p
	}
	if ix >= p.Ix+len(p.Data) {
		panic("index too large")
	}
	relIx := ix - p.Ix
	split := sort.SearchInts(p.LineBreaks, relIx)
	newPiece := &piece{
		Ix:         ix,
		LineBreaks: p.LineBreaks[split:],
		Dead:       p.Dead,
		Data:       p.Data[relIx:],
		next:       p.next,
		prev:       p,
	}
	for i := range newPiece.LineBreaks {
		newPiece.LineBreaks[i] -= relIx
	}
	p.next.prev = newPiece
	p.next = newPiece
	p.Data = p.Data[:relIx]
	p.LineBreaks = p.LineBreaks[:split]
	if p.Dead {
		newPiece.LineNumber = p.LineNumber
	} else {
		newPiece.LineNumber = p.LineNumber + split
	}
	return newPiece
}

func (p *piece) kill() {
	p.Dead = true
	p.next.updateIndices(0, -len(p.LineBreaks))
}

func (right *piece) insertBefore(b []byte, dead bool) {
	left := right.prev

	p := &piece{
		Ix:         right.Ix,
		LineNumber: right.LineNumber,
		LineBreaks: IndexAll(b, []byte("\n")),
		Dead:       dead,
		Data:       b,
		next:       right,
		prev:       left,
	}

	left.next = p
	right.prev = p

	nLines := 0
	if !p.Dead {
		nLines = len(p.LineBreaks)
	}

	right.updateIndices(len(p.Data), nLines)
}

func (p *piece) updateIndices(bytes, lines int) {
	if p == nil {
		return
	}
	p.Ix += bytes
	p.LineNumber += lines

	p.next.updateIndices(bytes, lines)
}

func (p *piece) mergeLeft() *piece {
	left := p.prev
	n := len(left.Data)
	data := make([]byte, len(left.Data)+len(p.Data))
	copy(data, left.Data)
	copy(data[len(left.Data):], p.Data)
	left.Data = data
	lineBreaks := p.LineBreaks
	for i := range lineBreaks {
		lineBreaks[i] += n
	}
	left.LineBreaks = append(left.LineBreaks, lineBreaks...)
	left.next = p.next
	left.next.prev = left

	return left
}
