package weeded

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestOT(t *testing.T) {
	doc1 := NewPieceChain([]byte("Hello World!"))
	doc2 := NewPieceChain([]byte("Hello World!"))

	op1 := Op{{N: 6}, {N: 5, S: []byte("wide ")}, {N: 6}}
	op2 := Op{{N: doc1.Length()}, {N: 10, S: []byte(" and stuff")}}

	err := doc1.Apply(op1)
	if err != nil {
		t.Fatal(err, doc1.Length())
	}
	err = doc2.Apply(op2)
	if err != nil {
		t.Error(err)
	}

	b, _ := ioutil.ReadAll(doc1.NewReader())

	b, _ = ioutil.ReadAll(doc2.NewReader())

	op1t, op2t, err := Transform(op1, op2)
	if err != nil {
		t.Error(err)
	}

	err = doc1.Apply(op2t)
	if err != nil {
		t.Error(err)
	}
	err = doc2.Apply(op1t)
	if err != nil {
		t.Error(err)
	}

	b1, _ := ioutil.ReadAll(doc1.NewReader())
	b2, _ := ioutil.ReadAll(doc2.NewReader())

	if !bytes.Equal(b1, b2) {
		t.Fatal("strings do not match")
	}

	op1inv := op1.Inverse()
	op1inv, _, err = Transform(op1inv, op2t)
	if err != nil {
		t.Error(err)
	}

	err = doc1.Apply(op1inv)
	if err != nil {
		t.Error(err)
	}

	op2inv := op2t.Inverse()
	op2inv, _, err = Transform(op2inv, op1inv)
	if err != nil {
		t.Error(err)
	}

	err = doc1.Apply(op2inv)
	if err != nil {
		fmt.Println(op2inv.Count())
		t.Error(err)
	}

	b, _ = ioutil.ReadAll(doc1.NewReader())
	if string(b) != "Hello World!" {
		t.Fatal("result is not \"Hello World!\", but \"" + string(b) + "\"")
	}

	doc1 = NewPieceChain([]byte("Hello World!"))
	v := doc1.View(2, 10)
	b, _ = ioutil.ReadAll(v.NewReader())

	if string(b) != "llo Worl" {
		t.Fatal("result is not \"llo Worl\", but \"" + string(b) + "\"")
	}

	err = v.Apply(op1)
	if err != nil {
		t.Error(err)
	}
	b, _ = ioutil.ReadAll(v.NewReader())

	if string(b) != "llo wide Worl" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	op2 = Op{{N: -5, S: []byte("Hello")}, {N: 7}}
	_, op2, err = Transform(op1, op2)
	if err != nil {
		t.Error(err)
	}
	v.Apply(op2)

	b, _ = ioutil.ReadAll(v.NewReader())
	if string(b) != " wide Worl" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	op3 := Op{}
	op3 = op3.Retain(5)
	op3 = op3.Delete([]byte(" World"))
	op3 = op3.Retain(1)
	_, op3, err = Transform(op1, op3)
	if err != nil {
		t.Error(err)
	}
	_, op3, err = Transform(op2, op3)
	if err != nil {
		t.Error(err)
	}
	v.Apply(op3)
	b, _ = ioutil.ReadAll(v.NewReader())
	if string(b) != "wide " {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}
}

func TestReader(t *testing.T) {
	doc := NewPieceChain(nil)

	op := Op{}.Retain(doc.Length()).Insert([]byte("line 1\n"))

	doc.Apply(op)

	op = Op{}.Retain(doc.Length()).Insert([]byte("line 2"))

	doc.Apply(op)

	op = Op{}.Retain(doc.Length()).Insert([]byte("\nline 3\n"))

	doc.Apply(op)

	rd := doc.NewReader()
	b, _ := ioutil.ReadAll(rd)
	if string(b) != "line 1\nline 2\nline 3\n" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	rd.MoveToLine(0)
	b, _ = ioutil.ReadAll(rd)
	if string(b) != "line 1\nline 2\nline 3\n" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	rd.MoveToLine(1)
	b, _ = ioutil.ReadAll(rd)
	if string(b) != "line 2\nline 3\n" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	rd.MoveToLine(2)
	b, _ = ioutil.ReadAll(rd)
	if string(b) != "line 3\n" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	rd.MoveToLine(3)
	b, _ = ioutil.ReadAll(rd)
	if string(b) != "" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	op = Op{}.Retain(7).Delete([]byte("line 2\n")).Retain(7)
	doc.Apply(op)
	rd = doc.NewReader()
	rd.MoveToLine(1)
	b, _ = ioutil.ReadAll(rd)
	if string(b) != "line 3\n" {
		t.Fatal("result should not be \"" + string(b) + "\"")
	}

	cells := rd.Render(0, 0, 5, 5)
	rd.MoveToLine(0)
	b, _ = ioutil.ReadAll(rd)
	fmt.Println(cells)
}
