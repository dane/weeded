package main

import (
	"log"
	"os"

	"github.com/gdamore/tcell"
	"gitlab.com/dane/weeded"
)

var lg *log.Logger
var root string

func init() {
	lg = log.New(os.Stderr, "Error: ", 0)
}

func main() {
	filename := ""
	if len(os.Args) >= 2 {
		filename = os.Args[1]
	}
	buf, err := weeded.NewBuffer(filename)
	if err != nil {
		lg.Fatal(err)
	}

	screen, err := tcell.NewScreen()
	if err != nil {
		lg.Fatal(err)
	}
	err = screen.Init()
	if err != nil {
		lg.Fatal(err)
	}
	width, height := screen.Size()

	v := buf.NewView(width, height)

	for {
		event := screen.PollEvent()
		switch e := event.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyESC {
				return
			}
			switch e.Rune() {
			case 'l':
				v.MoveRight()
			case 'h':
				v.MoveLeft()
			case 'j':
				v.MoveDown()
			case 'k':
				v.MoveUp()
			case 'x':
				v.DeleteRune()
			}
		case *tcell.EventResize:
			width, height := e.Size()
			v.Resize(width, height)
		}
		screen.Clear()
		cells := v.Cells()
		for i, l := range cells {
			for j, c := range l {
				screen.SetContent(j, i, c.R, nil, tcell.StyleDefault)
			}
		}
		X, Y := v.Cursor()
		screen.ShowCursor(X, Y)
		screen.Show()
	}
}
