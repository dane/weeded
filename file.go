// +build ignore

package weeded

import (
	"encoding/json"
	"io/ioutil"
	"log"

	"github.com/dane-unltd/msglog"
)

type OpMsg struct {
	ID uint64
	Ix uint64
	Op Op
}

type Buffer struct {
	filename string

	otLog    *msglog.Log
	consumer *msglog.Consumer
	buf      *PieceChain
	ots      chan OpMsg
	view     chan chan *PieceChain
	quit     chan chan struct{}
	nextIx   uint64
}

func NewBuffer(filename string) (*Buffer, error) {
	l, err := msglog.Recover(filename + ".master.weeded")
	if err != nil {
		return nil, err
	}
	f := &Buffer{
		otLog: l,
	}

	c, err := l.Consumer()
	if err != nil {
		return nil, err
	}

	var op Op
	for c.HasNext() {
		_, err := c.Next()
		if err != nil {
			return nil, err
		}
		pl, err := c.Payload()
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(pl, &op)
		if err != nil {
			return nil, err
		}

		f.buf, err = op.ApplyTo(f.buf)
		if err != nil {
			return nil, err
		}
		f.nextIx++
	}
	f.consumer = c
	f.ots = make(chan OtMsg)
	f.full = make(chan chan []byte)
	f.quit = make(chan chan struct{})
	f.filename = filename

	go f.controller()

	return f, nil
}

func (f *Buffer) controller() {
	c := f.consumer
	for {
		select {
		case otmsg := <-f.ots:
			var err error
			op := otmsg.Op
			if otmsg.Ix < f.nextIx {
				seq := otmsg.Ix
				err = c.Goto(uint64(seq))
				if err != nil {
					log.Println(err)
					f.closeAll()
					return
				}

				var oldop Op
				for seq < f.nextIx {
					_, err := c.Next()
					if err != nil {
						log.Println(err)
						f.closeAll()
						return
					}
					pl, err := c.Payload()
					if err != nil {
						log.Println(err)
						f.closeAll()
						return
					}
					err = json.Unmarshal(pl, &oldop)
					if err != nil {
						log.Println(err)
						f.closeAll()
						return
					}
					op, _, err = Transform(op, oldop)
					if err != nil {
						log.Println(err)
						f.closeAll()
						return
					}
					seq++
				}
			}

			err = f.buf.Apply(op)
			if err != nil {
				log.Println(err)
				f.closeAll()
				return
			}

			buf, err := json.Marshal(op)
			if err != nil {
				log.Println(err)
				f.closeAll()
				return
			}
			f.otLog.Push(msglog.Msg{From: op.UID}, buf)
			f.nextIx++

		case ret := <-f.full:
			retBuf := make([]byte, len(f.buf))
			copy(retBuf, f.buf)
			ret <- retBuf
		case ret := <-f.quit:
			f.closeAll()
			ret <- struct{}{}
			return
		}
	}
}

func (f *Buffer) Apply(op Op, ix int64) {
	f.ots <- OtMsg{Op: op, Ix: ix}
}

func (f *Buffer) Bytes() []byte {
	ret := make(chan []byte)
	f.full <- ret
	return <-ret
}

func (f *Buffer) Close() {
	ret := make(chan struct{})
	f.quit <- ret
	<-ret
}

func (f *Buffer) closeAll() {
	f.consumer.Close()
	f.otLog.Close()
	err := ioutil.WriteBuffer(f.filename, f.buf, 0744)
	if err != nil {
		log.Println(err)
	}
}
