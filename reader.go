package weeded

import (
	"fmt"
	"io"
	"sort"
	"unicode/utf8"
)

type PieceChainReader struct {
	ix int
	p  *piece
}

func (pc PieceChain) NewReader() *PieceChainReader {
	return &PieceChainReader{0, pc.head}
}

func (pr *PieceChainReader) Ix() int {
	return pr.ix
}

func (pr *PieceChainReader) MoveToLine(n int) error {
	for n <= pr.p.LineNumber && pr.p.prev != nil {
		pr.p = pr.p.prev
	}
	if n == 0 {
		pr.ix = 0
		return nil
	}
	if n <= pr.p.LineNumber {
		pr.ix = pr.p.Ix
		return io.EOF
	}
	for pr.p.next != nil && pr.p.next.LineNumber < n {
		pr.p = pr.p.next
	}
	if pr.p.next == nil {
		pr.ix = pr.p.Ix
		return io.EOF
	}
	offset := pr.p.LineBreaks[n-pr.p.LineNumber-1]
	pr.ix = pr.p.Ix + offset + 1
	return nil
}

func (pr *PieceChainReader) MoveTo(ix int) int {
	pr.ix = ix
	for ix < pr.p.Ix && pr.p.prev != nil {
		pr.p = pr.p.prev
	}
	if ix < pr.p.Ix {
		pr.ix = pr.p.Ix
		return pr.ix
	}
	for pr.ix >= pr.p.Ix+len(pr.p.Data) && pr.p.next != nil {
		pr.p = pr.p.next
	}
	if pr.ix >= pr.p.Ix+len(pr.p.Data) {
		pr.ix = pr.p.Ix
		return pr.ix
	}
	if !pr.p.Dead {
		return pr.ix
	}
	for pr.p.Dead && pr.p.next != nil {
		pr.p = pr.p.next
	}
	pr.ix = pr.p.Ix
	return pr.ix
}

func (pr *PieceChainReader) Line() int {
	if len(pr.p.LineBreaks) == 0 {
		return pr.p.LineNumber
	}
	lb := sort.SearchInts(pr.p.LineBreaks, pr.ix-pr.p.Ix)
	return pr.p.LineNumber + lb
}

func (pr *PieceChainReader) PeekSlice() ([]byte, error) {
	p := pr.p
	for (pr.ix >= p.Ix+len(p.Data) || p.Dead) && p.next != nil {
		p = p.next
	}
	pr.p = p
	if pr.ix >= p.Ix+len(p.Data) {
		return nil, io.EOF
	}
	if p.next == nil {
		return nil, io.EOF
	}
	if pr.ix < p.Ix {
		pr.ix = p.Ix
	}
	relIx := pr.ix - p.Ix

	return p.Data[relIx:], nil
}

func (pr *PieceChainReader) Read(b []byte) (int, error) {
	s, err := pr.PeekSlice()
	if err != nil {
		return 0, err
	}
	n := copy(b, s)
	pr.ix += n
	return n, nil
}

func (pr *PieceChainReader) PeekRune() (r rune, ix int, err error) {
	var s []byte
	s, err = pr.PeekSlice()
	if err != nil {
		return
	}
	r, _ = rune(s[0]), 1
	if r >= utf8.RuneSelf {
		r, _ = utf8.DecodeRune(s)
	}
	ix = pr.ix
	return
}

func (pr *PieceChainReader) ReadRune() (r rune, ix int, err error) {
	var s []byte
	s, err = pr.PeekSlice()
	if err != nil {
		return
	}
	var size int
	r, size = rune(s[0]), 1
	if r >= utf8.RuneSelf {
		r, size = utf8.DecodeRune(s)
	}
	ix = pr.ix
	pr.ix += size
	return
}

func (pr *PieceChainReader) ReadPrevRune() (r rune, ix int, err error) {
	size := 0
	relIx := pr.ix - pr.p.Ix
	if !pr.p.Dead && relIx > 0 {
		fmt.Println("decoding from current piece", relIx)
		r, size = utf8.DecodeLastRune(pr.p.Data[:relIx])
		pr.ix -= size
		ix = pr.ix
		return
	}
	if pr.p.prev != nil {
		pr.p = pr.p.prev
	}
	for pr.p.Dead {
		pr.p = pr.p.prev
	}
	if pr.p.Data == nil {
		pr.ix = pr.p.Ix
		return 0, 0, io.EOF
	}
	fmt.Println("decoding from piece", pr.p)
	r, size = utf8.DecodeLastRune(pr.p.Data)
	pr.ix = pr.p.Ix + len(pr.p.Data) - size
	ix = pr.ix
	return
}

func (pr *PieceChainReader) ReadLine() ([]Cell, error) {
	var line []Cell
	for {
		r, ix, err := pr.ReadRune()
		if err != nil {
			return line, err
		}
		line = append(line, Cell{Ix: ix, R: r})
		if r == '\n' {
			return line, nil
		}
	}
}

func (pr *PieceChainReader) Lines(top, n int) Cells {
	var cells Cells

	for i := 0; i < n; i++ {
		err := pr.MoveToLine(top + i)
		if err != nil {
			return cells
		}
		line, err := pr.ReadLine()
		cells = append(cells, line)
		if err != nil {
			return cells
		}
	}
	return cells
}

func (pr *PieceChainReader) Render(left, top, width, height int) Cells {
	var cells Cells

	for i := 0; i < height; i++ {
		err := pr.MoveToLine(top + i)
		if err != nil {
			return cells
		}
		cells = append(cells, []Cell{})
		for pos := 0; pos < left+width; pos++ {
			r, ix, err := pr.ReadRune()
			if err != nil {
				return cells
			}
			if r == '\n' {
				break
			}
			if pos < left {
				continue
			}
			cells[i] = append(cells[i], Cell{Ix: ix, R: r})
		}
	}
	return cells
}

func (pr *PieceChainReader) RenderWrapped(ix, desiredY, width, height int) (Cells, int, int) {
	pr.MoveTo(ix)
	currentLine := pr.Line()
	pr.MoveToLine(currentLine)

	line, err := pr.ReadLine()
	X, Y := 0, 0
	if len(line) > 0 {
		ixPos := sort.Search(len(line), func(i int) bool { return line[i].Ix >= ix })
		Y = ixPos / width
		X = ixPos % width
	}

	var cells Cells
	for len(line) > width {
		cells = append(cells, line[:width])
		line = line[width:]
	}
	cells = append(cells, line)

	for Y < desiredY {
		currentLine--
		pr.MoveToLine(currentLine)
		line, err = pr.ReadLine()
		if len(line) == 0 {
			desiredY = Y
			break
		}
		var tmp Cells
		for len(line) > width {
			tmp = append(tmp, line[:width])
			line = line[width:]
		}
		tmp = append(tmp, line)
		if len(tmp) < desiredY-Y {
			cells = append(tmp, cells...)
			Y += len(tmp)
		} else {
			cells = append(tmp[len(tmp)-(desiredY-Y):], cells...)
			Y = desiredY
		}
		if err != nil {
			desiredY = Y
		}
	}
	if Y > desiredY {
		cells = cells[Y-desiredY:]
	}
	pr.MoveTo(ix)
	currentLine = pr.Line()
	for len(cells) < height {
		currentLine++
		pr.MoveToLine(currentLine)
		line, err = pr.ReadLine()
		if len(line) == 0 {
			break
		}
		for len(line) > width {
			cells = append(cells, line[:width])
			line = line[width:]
		}
		cells = append(cells, line)
		if err != nil {
			break
		}
	}
	if len(cells) > height {
		cells = cells[:height]
	}
	return cells, X, desiredY
}
