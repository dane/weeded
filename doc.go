package weeded

import (
	"errors"
)

type Doc struct {
	pc *PieceChain

	userIxs map[uint64]int
	hist    []Op
}

func NewDoc(initialData []byte) Doc {
	return Doc{pc: NewPieceChain(initialData), userIxs: make(map[uint64]int)}
}

func (d Doc) Length() int {
	return d.pc.Length()
}

func (d *Doc) Apply(uid uint64, ix int, op Op) (int, error) {
	minIx, ok := d.userIxs[uid]
	if !ok {
		minIx = -1
	}
	if ix < minIx {
		err := errors.New("Reference for op below user minimum")
		return 0, err
	}
	if ix > len(d.hist) {
		err := errors.New("index too large")
		return 0, err
	}

	op = op.Squeeze()
	for i := ix; i < len(d.hist); i++ {
		var err error
		_, op, err = Transform(d.hist[i], op)
		if err != nil {
			return 0, err
		}
	}

	err := d.pc.Apply(op)
	if err != nil {
		return 0, err
	}
	d.hist = append(d.hist, op)

	d.userIxs[uid] = len(d.hist) - 1

	return d.userIxs[uid], nil
}

func (d *Doc) NewReader() *PieceChainReader {
	return d.pc.NewReader()
}
