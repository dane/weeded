package weeded

import (
	"unicode/utf8"
)

type View struct {
	id uint32

	width, height int

	cells Cells

	b  *Buffer
	pc PieceChainView
	rd *PieceChainReader

	cursorIx         int
	cursorX, cursorY int

	updates chan Update
}

type Cell struct {
	R  rune
	Ix int
}
type Cells [][]Cell

func (b *Buffer) NewView(width, height int) *View {
	c := make(chan Update)
	b.connect <- c
	pv := (<-c).pv
	v := &View{
		id:      b.nextID,
		width:   width,
		height:  height,
		pc:      pv,
		rd:      b.pc.NewReader(),
		updates: c,
	}
	b.nextID++
	v.Render()
	return v
}

func (v *View) Render() {
	for {
		select {
		case u := <-v.updates:
			v.pc = u.pv
			v.rd = v.pc.NewReader()
			v.cursorIx = u.op.UpdateIx(v.cursorIx)
			v.cursorIx = v.rd.MoveTo(v.cursorIx)
		default:
			break
		}
	}
	v.cells, v.cursorX, v.cursorY = v.rd.RenderWrapped(v.cursorIx, v.cursorY, v.width, v.height)
}

func (v *View) Cells() Cells {
	return v.cells
}

func (v *View) Cursor() (int, int) {
	return v.cursorX, v.cursorY
}

func (v *View) Resize(width, height int) {
	v.width = width
	v.height = height
	v.Render()
}

func (v *View) MoveRight() {
	v.rd.MoveTo(v.cursorIx)
	r, _, _ := v.rd.ReadRune()
	_, ix, err := v.rd.ReadRune()
	if err == nil && r != '\n' {
		v.cursorIx = ix
	}
	v.Render()
}

func (v *View) MoveLeft() {
	v.rd.MoveTo(v.cursorIx)
	r, ix, err := v.rd.ReadPrevRune()
	if err == nil && r != '\n' {
		v.cursorIx = ix
	}
	v.Render()
}

func (v *View) MoveUp() {
	lines, _, _ := v.rd.RenderWrapped(v.cursorIx, 1, v.width, 2)
	if len(lines) == 2 {
		if v.cursorX >= len(lines[0]) {
			v.cursorX = len(lines[0]) - 1
		}
		v.cursorIx = lines[0][v.cursorX].Ix
		v.cursorY--
		if v.cursorY < 0 {
			v.cursorY = 0
		}
	}
	v.Render()
}

func (v *View) MoveDown() {
	lines, _, _ := v.rd.RenderWrapped(v.cursorIx, 0, v.width, 2)
	if len(lines) == 2 {
		v.Render()
		if v.cursorX >= len(lines[1]) {
			v.cursorX = len(lines[1]) - 1
		}
		v.cursorIx = lines[1][v.cursorX].Ix
		v.cursorY++
		if v.cursorY == v.height {
			v.cursorY--
		}
	}
	v.Render()
}

func (v *View) DeleteRune() {
	if v.cursorX >= len(v.cells[v.cursorY]) {
		return
	}
	b := make([]byte, utf8.UTFMax)
	n := utf8.EncodeRune(b, v.cells[v.cursorY][v.cursorX].R)
	b = b[:n]
	op := Op{}.Retain(v.cursorIx).Delete(b).Retain(v.pc.Length() - v.cursorIx - len(b))
	v.Apply(op)
	v.Render()
}

func (v *View) Apply(op Op) {
	v.pc.Apply(v.id, op)
	u := <-v.updates
	v.pc = u.pv
	v.rd = v.pc.NewReader()
	v.cursorIx = u.op.UpdateIx(v.cursorIx)
	v.cursorIx = v.rd.MoveTo(v.cursorIx)
}

func (cs Cells) String() string {
	var s string
	for _, l := range cs {
		for _, c := range l {
			if string(c.R) != "\n" {
				s = s + string(c.R)
			}
		}
		s = s + "\n"
	}
	return s
}
